# %% [markdown]
"""
# Serializing a tiling
"""

# %% tags=['hide-cell']
from IPython import get_ipython

if get_ipython() is not None:
    get_ipython().run_line_magic("load_ext", "autoreload")
    get_ipython().run_line_magic("autoreload", "2")
    get_ipython().run_line_magic("matplotlib", "inline")


# %%
from tilings.dual import base as b
from matplotlib.patches import Rectangle
import matplotlib.pyplot as plt
import shapely.geometry as sg
from descartes import PolygonPatch
from pygsutils import general as g

# %%
# g.setup_logging("test.log")

# %%
tiling_seq = b.TilingSeq()

# %%
tiling_seq.add_tilings(num_steps=20)

# %%
tiling_seq.tilings[-1].as_json