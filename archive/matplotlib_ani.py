# %% [markdown]
"""
# Experiments with matplotlib animation
"""

# %% tags=['hide-cell']
from IPython import get_ipython

if get_ipython() is not None:
    get_ipython().run_line_magic("load_ext", "autoreload")
    get_ipython().run_line_magic("autoreload", "2")
    get_ipython().run_line_magic("matplotlib", "inline")


# %%
from tilings.dual import base as b
from pygifsicle import optimize


# %%
tiling_seq = b.TilingSeq()

# %%
tiling_seq.add_tilings(num_steps=500)

# %%
tiling_seq.save_gif("test.gif", num_tilings=150)