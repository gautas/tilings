# %% [markdown]
"""
# Debugging tiling sequence
"""

# %% tags=['hide-cell']
from IPython import get_ipython

if get_ipython() is not None:
    get_ipython().run_line_magic("load_ext", "autoreload")
    get_ipython().run_line_magic("autoreload", "2")
    get_ipython().run_line_magic("matplotlib", "inline")


# %%
from tilings.dual import base as b, deserialize as d
from matplotlib.patches import Rectangle
import matplotlib.pyplot as plt
from matplotlib.figure import Figure
import shapely.geometry as sg
from descartes import PolygonPatch
from pygsutils import general as g
import json
from typing import Dict
from tqdm import tqdm

# %%
# g.setup_logging("test.log")

# %%
b.Point(x=1.5, y=.2)

# %%
tiling_seq = b.TilingSeq()

# %%
seq_len = 300

# %%
for i in tqdm(range(seq_len)):
    with open(f"debug/{i}.json", "w") as f:
        json.dump(tiling_seq.next_as_json(tiling_seq.next), f)

# %%
jsons = []
for i in tqdm(range(seq_len)):
    with open(f"debug/{i}.json", "r") as f:
        jsons.append(json.load(f))


# %%
def tiling_image(json: Dict, title: str) -> Figure:
    tiling = d.json_to_tiling(json["tiling"])
    fig, ax = tiling.figax
    tiling.plot()

    tile = d.json_to_tile(json["tile"])
    new_tile = d.json_to_tile(json["new_tile"])

    if tile is not None:
        ax.add_patch(tile.custom_patch(fc=plt.cm.Dark2.colors[3]))
    ax.add_patch(new_tile.custom_patch(fc=plt.cm.Dark2.colors[4]))

    ax.text(2, 8, title)

    return fig

# %%
for i in tqdm(range(seq_len)):
    fig = tiling_image(json=jsons[i], title=str(i))
    fig.savefig(f"debug/images/{i}.png")
    plt.close()
# %%
