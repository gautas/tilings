import setuptools


with open("README.md", "r") as f:
    long_description = f.read()

setuptools.setup(
    name="tilings",
    version="0.0.1",
    author="Gautam Sisodia",
    packages=setuptools.find_packages(),
    classifiers=["Progamming Language :: Python :: 3"],
    install_requires=[
        "shapely==1.7.1",
        "numpy==1.20.2",
        "matplotlib==3.4.1",
        "descartes==1.1.0",
        "tqdm==4.60.0",
        "ipython==7.22.0",
        "google-auth==1.29.0",
        "google-cloud-storage==1.37.1",
        "tweepy==3.10.0",
        "networkx==2.5.1",
        "scipy==1.7.1",
    ]
)