jupytext --to ipynb book/contents/slides.py
jupyter-nbconvert --to slides --execute --log-level=10 --no-prompt --TagRemovePreprocessor.remove_input_tags={\"hide-input\"} book/contents/slides.ipynb
mv book/contents/slides.slides.html book/rendered/slides/slides.html
cp book/rendered/slides/slides.html book/_build/html/contents/
cp -r book/images book/_build/html/
rm book/contents/slides.ipynb