# %% [markdown] slideshow={"slide_type": "slide"}
"""
# A haphazard journey to a generative art twitter bot with matplotlib
"""

# %% tags=['hide-input']
from IPython import get_ipython

from tilings.dual import tiling

if get_ipython() is not None:
    get_ipython().run_line_magic("load_ext", "autoreload")
    get_ipython().run_line_magic("autoreload", "2")

# %% tags=['hide-input']
from IPython.display import HTML
from tilings.dual.slides import bucket, Example
from tilings.dual.bucket import TypeBlob, config
from dataclasses import dataclass
from tilings.dual.point import PointPolar
from tilings.dual.tile import Tile
from typing import List
from matplotlib import rc

rc("animation", html="html5")

config.set_loc_download("./../../bucket")

# %% tags=['hide-input']
HTML(f"<img src='{Example.TITLE.folder_local.fp_gif_book}'>")

# %% [markdown] slideshow={"slide_type": "slide"}
"""
## About me
- Gautam Sisodia
- NYC
- 6 yr old data scientist
- 2.5 yr old parent
"""


# %% [markdown] slideshow={"slide_type": "slide"}
"""
## Outline
1. what  
2. why  
3. how
4. when (bot examples)
5. where (in the code)
"""

# %% [markdown] slideshow={"slide_type": "slide"}
"""
## 1. what
### 1.1. tiles and tilings
- A **tiling** is a collection of shapes (called **tiles**) that fit together with no gaps and no overlaps
- Focused on
    - square and equilateral triangle tiles
    - **symmetric** tilings (symmetric around the center tile(s))

![example](./../images/tiling.png)
"""

# %% [markdown] slideshow={"slide_type": "subslide"}
"""
### 1.2. a python library to make and draw tilings
"""
# %%
from tilings.dual.sequence import TilingSeq

tiling_seq = TilingSeq(symmetric=True)
tiling_seq.add_tilings(num_steps=10)

# %%
tiling_seq.ani()

# %% [markdown] slideshow={"slide_type": "subslide"}
"""
### 1.3. a twitter bot that posts an animation of such a tiling daily
"""

# %% tags=['hide-input']
HTML(
    """
<blockquote class="twitter-tweet"><p lang="und" dir="ltr"><a href="https://t.co/udOuASLTDD">pic.twitter.com/udOuASLTDD</a></p>&mdash; tryling (@trylingbot) <a href="https://twitter.com/trylingbot/status/1425456830038302724?ref_src=twsrc%5Etfw">August 11, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 
"""
)


# %% [markdown] slideshow={"slide_type": "slide"}
"""
## 2. why
### 2.1 magnetic tiles toy
- how many different ways are there to tile?

<img src="./../images/PXL_20210424_232949123.jpg" alt="tiles" style="width: 600px;"/>
"""

# %% [markdown] slideshow={"slide_type": "subslide"}
"""
### 2.2 other generative art twitter accounts/bots
"""

# %% tags=['hide-input']
HTML(
    """
<blockquote class="twitter-tweet"><p lang="und" dir="ltr"><a href="https://t.co/k6jHNJj2OL">pic.twitter.com/k6jHNJj2OL</a></p>&mdash; incre.ment (@incre_ment) <a href="https://twitter.com/incre_ment/status/1378108227804520450?ref_src=twsrc%5Etfw">April 2, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 
"""
)


# %% [markdown] slideshow={"slide_type": "slide"}
"""
## 3. how
### 3.1. first attempt: pure Shapely
- library for geometric manipulation
- https://pypi.org/project/Shapely/ 
- seem to be floating point issues once tilings get big

![example](./../images/shapely_ex.png)
"""

# %% [markdown] slideshow={"slide_type": "subslide"}
"""
### 3.2. second attempt: dual graph
"""

# %% tags=['hide-input']
HTML(
    "<img align='left' width='45%' src='./../images/dual.gif'><img align='right' width='50%' src='./../images/constraints.png'>"
)


# %% [markdown] slideshow={"slide_type": "subslide"}
"""
### 3.3. third attempt: a mix

![centers](./../images/centers.gif)
"""

# %% [markdown] slideshow={"slide_type": "subslide"}
"""
### 3.4. Example
"""

# %%
from tilings.dual.sequence import TilingSeq

tiling_seq = TilingSeq(symmetric=True)
tiling_seq.add_tilings(num_steps=10)

# %%
tiling_seq.ani()

# %% [markdown] slideshow={"slide_type": "subslide"}
"""
### 3.5. algo gets stuck sometimes

![stuck](./../images/stuck.gif)
"""


# %% [markdown] slideshow={"slide_type": "slide"}
"""
## 4. when (bot examples)
### 4.1. stuck
"""

# %% tags=['hide-input']
print(Example.STUCK.date_str)

# %% tags=['hide-input']
HTML(f"<img src='{Example.STUCK.folder_local.fp_gif_book}'>")

# %% [markdown] slideshow={"slide_type": "subslide"}
"""
### 4.2. most squares
"""

# %% tags=['hide-input']
print(Example.MOST_SQUARES.date_str)

# %% tags=['hide-input']
HTML(f"<img src='{Example.MOST_SQUARES.folder_local.fp_gif_book}'>")

# %% [markdown] slideshow={"slide_type": "subslide"}
"""
### 4.3. most triangles
"""

# %% tags=['hide-input']
print(Example.MOST_TRIANGLES.date_str)

# %% tags=['hide-input']
HTML(f"<img src='{Example.MOST_TRIANGLES.folder_local.fp_gif_book}'>")


# %% [markdown] slideshow={"slide_type": "subslide"}
"""
### 4.4. most balanced
"""

# %% tags=['hide-input']
print(Example.HALF_AND_HALF.date_str)

# %% tags=['hide-input']
HTML(f"<img src='{Example.HALF_AND_HALF.folder_local.fp_gif_book}'>")

# %% [markdown] slideshow={"slide_type": "subslide"}
"""
### 4.5. when did it go viral?

From April 24, 2021 to August 26, 2021:
- 141 tweets
"""
# %% [markdown] slideshow={"slide_type": "fragment"}
"""
- 0 retweets
"""

# %% [markdown] slideshow={"slide_type": "fragment"}
"""
- 1 favorite (the most squares example)
"""

# %% [markdown] slideshow={"slide_type": "slide"}
"""
## 5. where (in the code)
### 5.1 points
"""

# %%
from tilings.dual.point import Point, PointPolar

point_ex = Point(x=1, y=1)

# %%
point_ex.distance(Point(x=2, y=1))

# %%
point_ex.polar

# %%
point_ex.point_sg  # as a shapely geometry Point

# %% [markdown] slideshow={"slide_type": "subslide"}
"""
### 5.2 tiles
"""

# %%
from tilings.dual.point import Point
from tilings.dual.tile import Square, Triangle

# %%
tile_ex = Triangle(center=Point(x=0, y=0), vertex=Point(x=1, y=1))
# %%
tile_ex.child_tiles[0]

# %%
tile_ex.polygon # as a shapely geometry Polygon


# %% [markdown] slideshow={"slide_type": "subslide"}
"""
### 5.3 tilings
"""

# %%
from tilings.dual.point import Point
from tilings.dual.tile import Square, Triangle
from tilings.dual.tiling import Tiling

# %%
tiling_ex = Tiling(
    tiles=[
        tile_ex,
        tile_ex.child_tiles[0],
        tile_ex.child_tiles[0].child_tiles[0],
    ]
)

# %%
tiling_ex.plot(extent=4, size=4)

# %% [markdown] slideshow={"slide_type": "subslide"}
"""
### 5.4 sequences of tilings
"""

# %%
from tilings.dual.sequence import TilingSeq

tiling_seq = TilingSeq(symmetric=True)
tiling_seq.add_tilings(num_steps=10)

# %%
tiling_seq.ani()


# %% [markdown] slideshow={"slide_type": "slide"}
"""
## Libraries used
All open source:
- [matplotlib](https://matplotlib.org)
- [shapely](https://shapely.readthedocs.io/en/stable/)
- [descartes](https://pypi.org/project/descartes/)
- [numpy](https://numpy.org/)
- [tweepy](https://docs.tweepy.org/en/stable/) for tweeting, requires twitter API key
- [google-cloud-storage](https://googleapis.dev/python/storage/latest/index.html) for saving tweet and metadata to cloud, requires google cloud key 
"""

# %% [markdown] slideshow={"slide_type": "slide"}
"""
## Hardware requirements
- daily pipeline takes ~ 3min on gitlab shared runners (3.75GB RAM)
    - clone and install package
    - make a symmetric tiling (run the algo for 70 steps)
    - make a gif of the tiling
    - post the gif to twitter
    - save the gif to a google cloud storage bucket
- daily generated gifs are about 2MB in size 
"""

# %% [markdown] slideshow={"slide_type": "slide"}
"""
## Thank you!
- repo: [gitlab.com/gautas/tilings](gitlab.com/gautas/tilings)
- website: [gautas.gitlab.io/tilings](gautas.gitlab.io/tilings)
- slides: [https://gautas.gitlab.io/tilings/contents/slides.html](https://gautas.gitlab.io/tilings/contents/slides.html)
- twitter bot: [twitter.com/trylingbot](twitter.com/trylingbot)
"""
