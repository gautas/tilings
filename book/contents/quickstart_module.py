# %% [markdown]
"""
# Quickstart: module
"""

# %% tags=['hide-cell']
from IPython import get_ipython
if get_ipython() is not None:
    get_ipython().run_line_magic('load_ext', 'autoreload')
    get_ipython().run_line_magic('autoreload', '2')

# %%
from tilings.dual import sequence
from IPython.display import HTML
from myst_nb import glue

# %%
tiling_seq = sequence.TilingSeq(symmetric=True)

# %%
tiling_seq.add_tilings(num_steps=30)

# %% tags=['hide-input']
tiling_seq.tilings[0].plot()

# %%
tiling_seq.tilings[-1].plot()

# %%
video = HTML(tiling_seq.ani(num_tilings=30).to_html5_video())

# %%
video

# %%
glue("video", video, display=False)