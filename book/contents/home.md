# tilings

A python library for generating random tilings of the plane by squares and triangles


```{glue:} video
```

[matplotlib](https://matplotlib.org/) is used to draw the tilings and create animations of the tilings being constructed like the video above.

## Install

Clone the [repo](https://gitlab.com/gautas/tilings) and run `pip install`:
```sh
git clone https://gitlab.com/gautas/tilings.git
pip install ./tilings/.
```


- [Repo](https://gitlab.com/gautas/tilings)
- {doc}`quickstart_script`
- {doc}`quickstart_module`
- {doc}`docs`
- {doc}`background`
- {doc}`tilings/rand`
- {doc}`slides`