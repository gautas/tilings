# %% [markdown]
"""
# Check tweeted tilings
"""

# %% tags=['hide-cell']
from IPython import get_ipython

if get_ipython() is not None:
    get_ipython().run_line_magic("load_ext", "autoreload")
    get_ipython().run_line_magic("autoreload", "2")

# %%
from tilings.dual.bucket import Bucket, TypeBlob, config
import json

# %%
bucket = Bucket()


# %%
bucket.setup_bucket(
    fp_cred_gcs="./../../keys/key.json",
    bucket_name="tilings",
)

# %%
config.set_loc_download("./../../bucket")

# %%
folders = bucket.folders

# %%
# largest square pcnt
sorted(
    filter(lambda x: x[1].has_meta, folders.items()),
    key=lambda x: x[1].pcnt_squares,
    reverse=True,
)[0]


# %%
# largest triangle pcnt
sorted(
    filter(lambda x: x[1].has_meta, folders.items()),
    key=lambda x: x[1].pcnt_squares,
)[0]


# %%
# closest to half-and-half
half_and_half = sorted(
    filter(lambda x: x[1].has_meta, folders.items()),
    key=lambda x: abs(0.5 - x[1].pcnt_squares),
)[0]

# %%
half_and_half

# %%
half_and_half[1].pcnt_squares

# %%
half_and_half[1].num_tiles, half_and_half[1].num_squares

# %%
