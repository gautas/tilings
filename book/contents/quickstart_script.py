# %% [markdown]
"""
# Quickstart: script

The script [`scripts/make_gif.py`](https://gitlab.com/gautas/tilings/-/blob/main/scripts/make_gif.py) creates a random tiling animation.

```sh
python scripts/make_gif.py
```

Below is the help output of the script which describes the arguments.
"""

# %%
import subprocess

print(
    subprocess.run(
        ["python", "./../../scripts/make_gif.py", "-h"], capture_output=True
    ).stdout.decode()
)
