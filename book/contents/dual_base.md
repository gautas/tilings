# tilings.dual.base

## Point

```{eval-rst}
.. autoclass:: tilings.dual.base.Point
    :show-inheritance:
    :members:
    :undoc-members:    
```

## PointPolar


```{eval-rst}
.. autoclass:: tilings.dual.base.PointPolar
    :show-inheritance:
    :members:
    :undoc-members:    
```

## Tile


```{eval-rst}
.. autoclass:: tilings.dual.base.Tile
    :show-inheritance:
    :members:
    :undoc-members:    
```

```{eval-rst}
.. autoclass:: tilings.dual.base.Triangle
    :show-inheritance:
```

```{eval-rst}
.. autoclass:: tilings.dual.base.Square
    :show-inheritance:
```


## Tiling
```{eval-rst}
.. autoclass:: tilings.dual.base.Tiling
    :show-inheritance:
    :members:
    :undoc-members:    
```

## TilingSeq
```{eval-rst}
.. autoclass:: tilings.dual.base.TilingSeq
    :show-inheritance:
    :members:
    :undoc-members:    
```

## NoTileAddable
```{eval-rst}
.. autoexception:: tilings.dual.base.NoTileAddable
```