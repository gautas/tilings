""" points, cartesian and polar
"""

from dataclasses import dataclass
from typing import TypeVar, Tuple, Dict
import numpy as np
import shapely.geometry as sg


@dataclass(eq=True, frozen=True)
class Point:
    """a cartesian point

    used for both the centers and vertices of tiles

    Example:

    >>> p = Point(x=1.5, y=.2)
    >>> p.y
    0.2
    """

    #: the x coordinate
    x: float
    #: the y coordinate
    y: float

    @property
    def polar(self) -> TypeVar("PointPolar"):
        """PointPolar: the polar form"""
        return PointPolar(
            r=np.sqrt(self.x ** 2 + self.y ** 2), theta=np.arctan2(self.y, self.x)
        )

    def translate(self, other: TypeVar("Point")) -> TypeVar("Point"):
        """get the point away by a vector represented by another point

        Args:
            other (Point): the point representing the translation vector

        Returns:
            Point: the translated point
        """
        return Point(x=self.x + other.x, y=self.y + other.y)

    @property
    def negative(self) -> TypeVar("Point"):
        """Point: the negative"""
        return Point(x=-1 * self.x, y=-1 * self.y)

    def distance(self, other: TypeVar("Point")) -> float:
        """the distance to another point

        Args:
            other (Point): the other point

        Returns:
            float: distance to other
        """
        return np.sqrt((self.x - other.x) ** 2 + (self.y - other.y) ** 2)

    @property
    def as_tuple(self) -> Tuple[float]:
        """Tuple[float]: x and y coordinates as a tuple"""
        return (self.x, self.y)

    @property
    def point_sg(self) -> sg.Point:
        """sg.Point -- the corresponding shapely geometry point"""
        return sg.Point((self.x, self.y))

    @property
    def max_abs_coord(self) -> float:
        """float: maximum coordinate absolute value"""
        return max(abs(self.x), abs(self.y))

    @property
    def as_json(self) -> Dict[str, float]:
        """Dict[str, float]: json representation"""
        return {"x": self.x, "y": self.y}


@dataclass
class PointPolar:
    """a polar point"""

    #: the r coordinate (distance from origin to point)
    r: float
    #: the theta coordinate (angle in radians to the point counter-clockwise from the xaxis)
    theta: float

    @property
    def cartesian(self) -> Point:
        """Point: the cartesian point"""
        return Point(x=self.r * np.cos(self.theta), y=self.r * np.sin(self.theta))

    def rotate(self, angle: float) -> TypeVar("PointPolar"):
        """the point rotated (counter-clockwise by angle)

        Args:
            angle (float): how much to rotate

        Returns:
            PointPolar: the rotated point
        """
        return PointPolar(r=self.r, theta=self.theta + angle)

    def scale(self, factor: float) -> TypeVar("PointPolar"):
        """the point scaled

        Args:
            factor (float): what to multiply by

        Returns:
            PointPolar: the scaled point
        """
        return PointPolar(r=self.r * factor, theta=self.theta)

    @property
    def as_json(self) -> Dict[str, float]:
        """Dict[str, float] -- json representation"""
        return {"r": self.r, "theta": self.theta}