"""a sequence of tilings
"""

from dataclasses import dataclass
from typing import Optional, List, Generator, Tuple
from functools import cached_property
from .tiling import Tiling, NoTileAddable
from .tile import Tile
from .utils import plot_setup
from .seed import Seed, get_random_seed
from .bucket import Bucket
import logging
from tqdm import tqdm
import numpy as np
from matplotlib.figure import Figure
from matplotlib.axes import Axes
from matplotlib.patches import PathPatch
from matplotlib.animation import ArtistAnimation, PillowWriter
from google.oauth2 import service_account
from google.cloud import storage
import tweepy
from datetime import datetime as dt


@dataclass
class TilingSeq:
    """a sequence of tilings"""

    #: if True, tilings are symmetric around the first tile
    symmetric: Optional[bool] = False

    @cached_property
    def tilings(self) -> List[Tiling]:
        """List[Tiling]: the tilings"""
        return []

    @property
    def size(self) -> int:
        """
        Returns:
            int: the number of tilings in the sequence
        """
        return len(self.tilings)

    def tiles_symmetric(self, tile: Tile) -> List[Tile]:
        if self.symmetric:
            tiles_symmetric = [
                tile.rotate_around_origin(angle=i * self.seed.angle_symmetry)
                for i in range(self.seed.num_sides)
            ]
        else:
            tiles_symmetric = [tile]
        return tiles_symmetric

    def gen_tiling_(
        self, tiling: Optional[Tiling] = None
    ) -> Generator[Tiling, None, None]:
        """generate tilings by adding compatible child tiles

        Searches recursively for next tilings, as you may reach a state where a tile is not surrounded yet by other tiles but no child tiles are compatible.

        Raises:
            NoTileAddable: if the focus tile is not surrounded but has no compatible child tiles
        """
        if tiling is None:
            self.seed = get_random_seed()
            tiling_new = self.seed.tiling
            yield tiling_new
            yield from self.gen_tiling_(tiling_new)
        tile_added = False
        for tile in tiling.tiles:
            if not tiling.internal(tile):
                for child_tile in tiling.child_tiles_compatible(tile):
                    try:
                        tile_added = True
                        tiling_new = tiling.add(self.tiles_symmetric(child_tile))
                        yield tiling_new
                        yield from self.gen_tiling_(tiling_new)
                    except NoTileAddable:
                        pass
                if not tile_added:
                    raise NoTileAddable

    @cached_property
    def gen_tiling(self) -> Generator[Tiling, None, None]:
        return self.gen_tiling_()

    def add_tiling(self) -> None:
        """add a tiling to the sequence that is the latest tiling with a random added tile"""
        self.tilings.append(next(self.gen_tiling))

    def add_tilings(self, num_steps: int) -> None:
        """add a number of tilings

        Args:
            num_steps (int): how many new tilings to add
        """
        logging.info(f"adding {num_steps} tilings to the sequence")
        for _ in tqdm(range(num_steps)):
            self.add_tiling()

    def tilings_interval(self, num_tilings: int) -> List[Tiling]:
        """List[Tiling]: sample from the tilings the given number evenly spaced out

        Args:
            num_tilings (int): number of tilings to get
        """
        interval = int(np.ceil(1.0 * len(self.tilings) / num_tilings))
        return [self.tilings[i] for i in range(len(self.tilings)) if i % interval == 0]

    def artists(
        self, ax: Axes, num_tilings: Optional[int] = None
    ) -> List[List[PathPatch]]:
        """List[List[PathPatch]]: artists of all the tilings

        Args:
            ax (Axes): axes to draw on
            num_tilings (Optional[int], optional): number of tilings to get artists for. Defaults to None (all tilings).
        """
        tilings = (
            self.tilings
            if num_tilings is None
            else self.tilings_interval(num_tilings=num_tilings)
        )
        return [tiling.artists(ax) for tiling in tilings]

    @property
    def figax(self) -> Tuple[Figure, Axes]:
        """Tuple[Figure, Axes]: figure and axes for plotting"""
        return plot_setup(extent=10)

    def ani(self, num_tilings: Optional[int] = None) -> ArtistAnimation:
        """
        Args:
            num_tilings (Optional[int]): number of tilings in the animation. Defaults to None (all tilings)

        Returns:
             ArtistAnimation: animation of the tiling sequence
        """
        fig, ax = self.figax
        artists = self.artists(ax, num_tilings=num_tilings)
        artists_and_reverse = artists + artists[::-1]
        return ArtistAnimation(fig, artists_and_reverse)

    def save_gif(self, fp: str, num_tilings: Optional[int]) -> None:
        """save the animation gif to file

        Args:
            fp (str): output file path
            num_tilings (Optional[int]): number of tilings to include in the animation
        """
        logging.info(f"saving the gif to {fp}")
        writer = PillowWriter(fps=10)
        self.ani(num_tilings=num_tilings).save(fp, writer=writer)
        self.fp_gif = fp

    @cached_property
    def bucket(self) -> Bucket:
        return Bucket()

    def setup_bucket(
        self, fp_cred_gcs: Optional[str], bucket_name: Optional[str]
    ) -> None:
        """setup the google cloud bucket for storing the gif and metadata

        Args:
            fp_cred_gcs (Optional[str]): path to the credentials file
            bucket_name (Optional[str]): name of the bucket
        """
        self.bucket.setup_bucket(fp_cred_gcs=fp_cred_gcs, bucket_name=bucket_name)

    @cached_property
    def loc_gcs(self) -> str:
        return dt.now().strftime("%y%m%d")

    @cached_property
    def blob_gcs_gif(self) -> Optional[storage.Blob]:
        return self.bucket.blob_gcs_gif(self.loc_gcs)

    def store_gif(self) -> None:
        """save the gif to the google cloud bucket"""
        if self.blob_gcs_gif is not None:
            logging.info(f"storing the gif in the google cloud storage bucket")
            self.blob_gcs_gif.upload_from_filename(self.fp_gif)

    @cached_property
    def blob_gcs_meta(self) -> Optional[storage.Blob]:
        return self.bucket.blob_gcs_gif(self.loc_gcs)

    def store_meta(self) -> None:
        """save the metadata to the google cloud bucket"""
        if self.blob_gcs_gif is not None:
            logging.info(
                f"storing the tiling sequence metadata in the google cloud storage bucket"
            )
            self.blob_gcs_meta.upload_from_string(
                str([tile.as_json for tile in self.tilings])
            )

    def setup_twitter(
        self,
        twitter_api_key: Optional[str],
        twitter_api_secret_key: Optional[str],
        twitter_access_token: Optional[str],
        twitter_access_secret_token: Optional[str],
    ) -> None:
        """setup twitter access for tweeting the gif

        Args:
            twitter_api_key (str): API key
            twitter_api_secret_key (str): API key secret
            twitter_access_token (str): Access token
            twitter_access_secret_token (str): Access token secret
        """
        self.twitter_api_key = twitter_api_key
        self.twitter_api_secret_key = twitter_api_secret_key
        self.twitter_access_token = twitter_access_token
        self.twitter_access_secret_token = twitter_access_secret_token

    @property
    def auth(self) -> Optional[tweepy.OAuthHandler]:
        if self.twitter_api_key is not None:
            auth = tweepy.OAuthHandler(
                self.twitter_api_key, self.twitter_api_secret_key
            )
            auth.set_access_token(
                self.twitter_access_token, self.twitter_access_secret_token
            )
        else:
            auth = None
        return auth

    @property
    def api(self) -> Optional[tweepy.API]:
        return tweepy.API(self.auth) if self.auth is not None else None

    def tweet_gif(self) -> None:
        """tweet the gif"""
        if self.api is not None:
            tweet = self.api.update_with_media(self.fp_gif)
            self.tweet_info = tweet._json
        else:
            self.tweet_info = None

    @cached_property
    def blob_gcs_tweet(self) -> Optional[storage.Blob]:
        return self.bucket.blob_gcs_tweet(self.loc_gcs)

    def store_tweet(self) -> None:
        """store tweet info in the google cloud bucket"""
        if self.tweet_info is not None and self.blob_gcs_tweet is not None:
            logging.info(f"storing the tweet in the google cloud storage bucket")
            self.blob_gcs_tweet.upload_from_string(str(self.tweet_info))