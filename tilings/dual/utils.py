"""general utilities for the dual method
"""

from typing import Tuple, List
import matplotlib.pyplot as plt
from matplotlib.figure import Figure
from matplotlib.axes import Axes
from matplotlib.patches import Rectangle
from .tile import Tile
from matplotlib.collections import PathCollection

def plot_setup(extent: float, size: float = 5) -> Tuple[Figure, Axes]:
    """setup a figure and axes for drawing a tiling

    Args:
        extent (float): the x and y axis limit

    Returns:
        Tuple[Figure, Axes]: the setup figure and axes
    """
    fig = plt.figure(figsize=(size, size))
    ax = plt.axes([0, 0, 1, 1], frameon=False)
    ax.set_xlim(left=-extent, right=extent)
    ax.set_ylim(bottom=-extent, top=extent)
    ax.axis("off")
    ax.add_patch(
        Rectangle(
            (-extent, -extent),
            2 * extent,
            2 * extent,
            color="black",  # plt.cm.Set2.colors[0],
            zorder=-2,
        )
    )
    return fig, ax


def artist_centers(tiles: List[Tile], ax: Axes, alpha: float = 1) -> PathCollection:
    return ax.scatter(
        [tile.center.x for tile in tiles],
        [tile.center.y for tile in tiles],
        color=plt.cm.Dark2.colors[2],
        s=100,
        alpha=alpha,
        zorder=2,
    )