"""tiles: general, square and triangle
"""

from dataclasses import dataclass
from typing import Optional, Tuple, TypeVar, Set, List, Union, Dict
from .point import Point, PointPolar
import numpy as np
from functools import cached_property
import random
from descartes import PolygonPatch
from matplotlib.patches import RegularPolygon
import matplotlib.pyplot as plt
import shapely.geometry as sg


@dataclass
class Tile:
    """a polygon in a tiling"""

    #: the center of the tile
    center: Optional[Point] = None
    #: a vertex on the tile
    vertex: Optional[Point] = None

    def __post_init__(self):
        """assign default values if no center or vertex passed"""
        if self.center is None or self.vertex is None:
            self.center = Point(0, 0)
            self.vertex = Point(0, self.center_to_vertex)

    @property
    def color(self) -> Tuple[float]:
        """Tuple[float]: the color of the tile"""
        pass

    @property
    def num_sides(self) -> int:
        """int: the number of sides of the tile"""
        pass

    @property
    def angle_symmetry(self) -> float:
        return 2 * np.pi / self.num_sides

    def rotate_around_origin(self, angle: float) -> TypeVar("Tile"):
        return self.__class__(
            center=self.center.polar.rotate(angle).cartesian,
            vertex=self.vertex.polar.rotate(angle).cartesian,
        )

    def rotate_around_center(
        self, angle: float, scale: float = 1, vertex: Optional[Point] = None
    ) -> Point:
        """the point corresponding to rotating and scaling a vertex of the tile around the center of the tile

        Args:
            angle (float): angle to rotate by
            scale (float, optional): factor to scale by. Defaults to 1.
            vertex (Optional[Point], optional): reference vertex. Defaults to None (in which case self.vertex is used).

        Returns:
            Point: the rotated point
        """
        if vertex is None:
            vertex = self.vertex
        return (
            vertex.translate(self.center.negative)
            .polar.rotate(angle)
            .scale(scale)
            .cartesian.translate(self.center)
        )

    @property
    def center_to_edge_mdpt(self) -> float:
        """
        float: distance from the center to the midpoint of an edge
        """
        pass

    @property
    def min_distance_to_other_center(self) -> float:
        """
        float: smallest possible distance from the center to the center of another tile
        """
        pass

    @property
    def other_vertices(self) -> Set[Point]:
        """
        Set[Point]: all tile vertices besides the specified vertex
        """
        pass

    @property
    def vertices(self) -> Set[Point]:
        """
        Set[Point]: all tile vertices
        """
        return self.other_vertices.union(set([self.vertex]))

    @property
    def vertices_sorted(self) -> List[Point]:
        """
        List[Point]: all tile vertices sorted by angle to the center
        """
        return sorted(
            self.vertices, key=lambda v: v.translate(self.center.negative).polar.theta
        )

    @property
    def vertices_sg(self) -> List[sg.Point]:
        """
        List[sg.Point]: tile vertices as shapely points
        """
        return [v.point_sg for v in self.vertices]

    @property
    def polygon(self) -> sg.Polygon:
        """
        sg.Polygon: the tile as a shapely polygon
        """
        return sg.MultiPoint(self.vertices_sg).convex_hull

    @property
    def child_tiles_by_vertex(self) -> List[List[TypeVar("Tile")]]:
        """List[List[TypeVar("Tile")]]: all possible adjacent tiles grouped by the shared vertex"""
        pass

    @cached_property
    def child_tiles(self) -> List[TypeVar("Tile")]:
        """
        List[TypeVar("Tile")]: all possible adjacent tiles
        """
        return [
            tile
            for tiles in self.child_tiles_by_vertex
            for tile in random.sample(tiles, len(tiles))
        ]

    def custom_patch(
        self, fc: Union[str, Tuple], ec: Union[str, Tuple] = "black", alpha: float = 1
    ) -> PolygonPatch:
        """the tile as a shapely PolygonPatch

        Args:
            fc (Union[str, Tuple]): face color
            ec (Union[str, Tuple], optional): edge color. Defaults to "black".
            alpha (float, optional): transparency value. Defaults to 1.

        Returns:
            PolygonPatch: the tile as a PolygonPatch
        """
        return PolygonPatch(
            self.polygon,
            ec=ec,
            fc=fc,
            alpha=alpha,
            zorder=-1,
        )  # plt.cm.Set2.colors[2],

    def patch(self, alpha: float = 1) -> PolygonPatch:
        """
        PolygonPatch: the tile as a shapely PolygonPatch with the color of the tile as face color
        """
        return self.custom_patch(
            ec="black", fc=self.color, alpha=alpha
        )  # plt.cm.Set2.colors[2],

    @property
    def reg_polygon(self) -> RegularPolygon:
        """
        RegularPolygon: the tile as a matplotlib RegularPolygon

        Currently not used, but could provide an alternative to using shapely for plotting.
        """
        RegularPolygon(
            xy=self.center.as_tuple,
            numVertices=self.num_sides,
            radius=self.center.distance(self.vertex),
            orientation=self.vertex.translate(self.center.negative).polar.theta,
        )

    @property
    def as_json(self) -> Dict:
        """
        Dict: the tile as a json (center, vertex and num_sides)
        """
        return {
            "center": self.center.as_json,
            "vertex": self.vertex.as_json,
            "num_sides": self.num_sides,
        }

    @property
    def center_to_vertex(self) -> float:
        pass


@dataclass
class Triangle(Tile):
    """an equilateral triangle tile"""

    @property
    def other_vertices(self) -> Set[Point]:
        return set(
            [
                self.rotate_around_center(2 * np.pi / 3),
                self.rotate_around_center(4 * np.pi / 3),
            ]
        )

    @property
    def color(self) -> Tuple[float]:
        return plt.cm.Dark2.colors[0]

    @property
    def num_sides(self) -> int:
        return 3

    @property
    def center_to_edge_mdpt(self) -> float:
        return 1 / (2 * np.sqrt(3))

    @property
    def center_to_vertex(self) -> float:
        return 1 / np.sqrt(3)

    @property
    def min_distance_to_other_center(self) -> float:
        return 1 / np.sqrt(3)

    @property
    def child_tiles_by_vertex(self) -> List[List[Tile]]:
        return [
            [
                Triangle(
                    center=self.rotate_around_center(angle=np.pi / 3, vertex=v),
                    vertex=v,
                ),
                Square(
                    center=self.rotate_around_center(
                        angle=np.pi / 3, scale=(np.sqrt(3) + 1) / 2, vertex=v
                    ),
                    vertex=v,
                ),
            ]
            for v in self.vertices_sorted
        ]


@dataclass
class Square(Tile):
    """a square tile"""

    @property
    def other_vertices(self) -> Set[Point]:
        return set(
            [
                self.rotate_around_center(np.pi / 2),
                self.rotate_around_center(np.pi),
                self.rotate_around_center(3 * np.pi / 2),
            ]
        )

    @property
    def num_sides(self) -> int:
        return 4

    @property
    def color(self) -> Tuple[float]:
        return plt.cm.Dark2.colors[1]

    @property
    def center_to_edge_mdpt(self) -> float:
        return 0.5

    @property
    def center_to_vertex(self) -> float:
        return np.sqrt(2) / 2

    @property
    def min_distance_to_other_center(self) -> float:
        return 0.5 + 1 / (2 * np.sqrt(3))

    @property
    def child_tiles_by_vertex(self) -> List[List[Tile]]:
        return [
            [
                Triangle(
                    center=self.rotate_around_center(
                        angle=np.pi / 4,
                        scale=(1 / np.sqrt(2)) + (1 / np.sqrt(6)),
                        vertex=v,
                    ),
                    vertex=v,
                ),
                Square(
                    center=self.rotate_around_center(
                        angle=np.pi / 4, scale=2 / np.sqrt(2), vertex=v
                    ),
                    vertex=v,
                ),
            ]
            for v in self.vertices_sorted
        ]