"""
google storage bucket
"""

from dataclasses import dataclass
from typing import Optional, List, TypeVar, Dict
from functools import cached_property
from google.oauth2 import service_account
from google.cloud import storage
from enum import Enum
import ast
import os
import json
from tqdm import tqdm
from pathlib import Path
import logging


@dataclass
class Config:
    loc_download: Optional[str] = "./bucket"

    def set_loc_download(self, loc: str) -> None:
        self.loc_download = loc


config = Config()


class TypeBlob(Enum):
    GIF = 1
    META = 2
    TWEET = 3

    def is_type(self, blob: TypeVar("Blob")) -> bool:
        return self.name.lower() in blob.name


@dataclass
class Blob:
    blob: storage.Blob

    @cached_property
    def name(self) -> str:
        return self.blob.name

    @property
    def name_folder(self) -> Optional[str]:
        if "/" in self.name:
            return self.name.split("/")[0]
        else:
            return None

    @property
    def type_blob(self) -> Optional[TypeBlob]:
        for type_blob in TypeBlob:
            if type_blob.is_type(self):
                return type_blob
        else:
            return None

    @cached_property
    def text(self) -> str:
        return self.blob.download_as_text()

    @cached_property
    def info(self) -> Dict:
        return ast.literal_eval(self.text)

    @property
    def fp_download(self) -> str:
        return f"{config.loc_download}/{self.name}"

    def download(self) -> None:
        Path(self.fp_download).parent.mkdir(parents=True, exist_ok=True)
        if not os.path.isfile(self.fp_download):
            logging.info(f"downloading to {self.fp_download}")
            if self.type_blob == TypeBlob.META:
                with open(self.fp_download, "w") as f:
                    json.dump(self.info, f)
            elif self.type_blob == TypeBlob.GIF:
                self.blob.download_to_filename(self.fp_download)
        else:
            logging.info(f"file found at {self.fp_download}")

    @cached_property
    def local(self) -> Dict:
        with open(self.fp_download, "r") as f:
            contents = json.load(f)
        return contents[-1]


@dataclass
class Folder:
    name: str

    def __post_init__(self) -> None:
        self.blobs = []

    def add(self, blob: Blob) -> None:
        self.blobs.append(blob)

    def get(self, type_blob: TypeBlob) -> Optional[Blob]:
        for blob in self.blobs:
            if blob.type_blob == type_blob:
                return blob
        else:
            return None

    @property
    def meta(self) -> Blob:
        return self.get(TypeBlob.META)

    @property
    def has_meta(self) -> Blob:
        return self.meta is not None

    @cached_property
    def num_tiles(self) -> int:
        return len(self.meta_local)

    @cached_property
    def num_squares(self) -> int:
        return len([tile for tile in self.meta_local if tile["num_sides"] == 4])

    @cached_property
    def num_triangles(self) -> int:
        return len([tile for tile in self.meta_local if tile["num_sides"] == 3])

    @cached_property
    def pcnt_squares(self) -> float:
        return float(self.num_squares) / float(self.num_tiles)

    @property
    def fp_gif_local(self) -> str:
        return f"{config.loc_download}/{self.name}/tiling_{self.name}.gif"

    @property
    def fp_gif_book(self) -> str:
        return f"./../images/tiling_{self.name}.gif"


@dataclass
class Bucket:
    def setup_bucket(
        self, fp_cred_gcs: Optional[str], bucket_name: Optional[str]
    ) -> None:
        """setup the google cloud bucket for storing the gif and metadata

        Args:
            fp_cred_gcs (Optional[str]): path to the credentials file
            bucket_name (Optional[str]): name of the bucket
        """
        self.fp_cred_gcs = fp_cred_gcs
        self.bucket_name = bucket_name

    @cached_property
    def cred_gcs(self) -> Optional[service_account.Credentials]:
        return (
            service_account.Credentials.from_service_account_file(self.fp_cred_gcs)
            if self.fp_cred_gcs is not None
            else None
        )

    @cached_property
    def client_gcs(self) -> Optional[storage.Client]:
        return (
            storage.Client(credentials=self.cred_gcs)
            if self.cred_gcs is not None
            else None
        )

    @cached_property
    def bucket_gcs(self) -> Optional[storage.Bucket]:
        return (
            self.client_gcs.get_bucket(self.bucket_name)
            if self.client_gcs is not None and self.bucket_name is not None
            else None
        )

    def blob_gcs_gif(self, loc_gcs: str) -> Optional[storage.Blob]:
        return (
            self.bucket_gcs.blob(f"{loc_gcs}/tiling_{loc_gcs}.gif")
            if self.bucket_gcs is not None
            else None
        )

    def blob_gcs_meta(self, loc_gcs: str) -> Optional[storage.Blob]:
        return (
            self.bucket_gcs.blob(f"{loc_gcs}/meta_{loc_gcs}.json")
            if self.bucket_gcs is not None
            else None
        )

    def blob_gcs_tweet(self, loc_gcs: str) -> Optional[storage.Blob]:
        return (
            self.bucket_gcs.blob(f"{loc_gcs}/tweet_{loc_gcs}.json")
            if self.bucket_gcs is not None
            else None
        )

    @cached_property
    def blobs_storage(self) -> List[storage.Blob]:
        return self.client_gcs.list_blobs(self.bucket_gcs)

    @cached_property
    def blobs(self) -> List[Blob]:
        return [Blob(blob) for blob in self.blobs_storage]

    @cached_property
    def folders(self) -> Dict[str, Folder]:
        folders = {}
        for blob in self.blobs:
            if blob.name_folder not in folders:
                folders[blob.name_folder] = Folder(name=blob.name_folder)
            folders[blob.name_folder].add(blob)

        return folders

    def download_meta(self) -> None:
        for key in tqdm(self.folders):
            self.folders[key].download_meta()

    # @cached_property
    # def metas_local(self) -> List[Dict]:
    #     return [folder.meta_local for folder in self.folders if folder.has_meta]