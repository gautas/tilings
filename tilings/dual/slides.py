"""
tools used for making the slides
"""

from enum import Enum
from typing import NamedTuple
from .bucket import Folder, Bucket
from functools import cached_property
from datetime import datetime


bucket = Bucket()

class ExampleParams(NamedTuple):
    name_folder: str


class Example(ExampleParams, Enum):
    TITLE = ExampleParams("210810")
    MOST_SQUARES = ExampleParams("210716")
    MOST_TRIANGLES = ExampleParams("210520")
    HALF_AND_HALF = ExampleParams("210517")
    STUCK = ExampleParams("210809")

    @cached_property
    def folder(self) -> Folder:
        return bucket.folders.get(self.name_folder)
    
    @cached_property
    def folder_local(self) -> Folder:
        return Folder(name=self.name_folder)

    @cached_property
    def date_str(self) -> str:
        return datetime.strptime(self.name_folder, "%y%m%d").strftime("%B %d, %Y")
