"""tools to deserialize from json representations
"""
from tilings.dual import base as b
from typing import Dict, List, Optional


def json_to_point(json: Dict) -> b.Point:
    return b.Point(x=json["x"], y=json["y"])


def json_to_tile(json: Dict) -> Optional[b.Tile]:
    if len(json) == 0:
        return None
    return (b.Triangle if json["num_sides"] == 3 else b.Square)(
        center=json_to_point(json["center"]), vertex=json_to_point(json["vertex"])
    )


def json_to_tiling(json: List[Dict]) -> b.Tiling:
    return b.Tiling(tiles=[json_to_tile(json_tile) for json_tile in json])
