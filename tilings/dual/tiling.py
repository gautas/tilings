"""tilings and seeds
"""

from dataclasses import dataclass
from typing import Optional, List, TypeVar, Tuple, Dict
from .tile import Tile, Triangle, Square
from .utils import plot_setup, artist_centers
from functools import cached_property
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.figure import Figure
from matplotlib.axes import Axes
from matplotlib.patches import PathPatch
from matplotlib.collections import LineCollection, PathCollection
from enum import Enum


@dataclass
class Tiling:
    """a collection of tiles"""

    tiles: Optional[List[Tile]] = None

    def __post_init__(self):
        """assign an empty list of tiles if none provided"""
        if self.tiles is None:
            self.tiles = []

    @property
    def size(self) -> int:
        """number of tiles"""
        return len(self.tiles)

    def add(self, tiles: List[Tile]) -> TypeVar("Tiling"):
        """add tiles

        Args:
            tiles (List[Tile]): tiles to add

        Returns:
            Tiling: new Tiling with added tiles
        """
        return Tiling(tiles=self.tiles + tiles)

    def contains(self, other: Tile) -> bool:
        """check if a tile is in the tiling

        Args:
            other (Tile): tile to check

        Returns:
            bool: True if the tile is in the tiling, otherwise False
        """
        for tile in self.tiles:
            if tile.center.distance(other.center) < 0.01:
                return True
        return False

    def num_child_tiles_contains(self, other: Tile) -> int:
        """int: the number of possible adjacent tiles to a given tile that are already in the tiling

        Args:
            other (Tile): the tile to check
        """
        return len([tile for tile in other.child_tiles if self.contains(tile)])

    def internal(self, other: Tile) -> bool:
        """bool: check if a tile is in the tiling and surrounded by tiles in the tiling

        Args:
            other (Tile): tile to check
        """
        return (
            self.contains(other)
            and self.num_child_tiles_contains(other) == other.num_sides
        )

    def consistent(self, other: Tile, level: int = 1, max_level: int = 3) -> bool:
        """bool: check if a candidate tile does not overlap current tiles and can possibly be surrounded by tiles that do not overlap current tiles, repeated for possible child tiles up to max_level.

        Args:
            other (Tile): tile to check
            level (int, optional): current level. Defaults to 1.
            max_level (int, optional): the maximum level to check. Defaults to 3.
        """
        consistent = True
        for tiles in other.child_tiles_by_vertex:
            consistent_vertex = False
            for tile in tiles:
                if (
                    self.contains(tile)
                    or not self.overlap(tile)
                    and (
                        level >= max_level
                        or self.consistent(
                            other=tile, level=level + 1, max_level=max_level
                        )
                    )
                ):
                    consistent_vertex = True
            if consistent_vertex is False:
                consistent = False
        return consistent

    def overlap(self, other: Tile) -> bool:
        """check if a tile overlaps with the tiling

        Args:
            other (Tile): tile to check

        Returns:
            bool: True if the tile overlaps, otherwise False
        """
        for tile in self.tiles:
            if (
                tile.center.distance(other.center)
                < tile.min_distance_to_other_center - 0.01
            ):
                return True
        return False

    def child_tiles_compatible(self, other: Tile) -> List[Tile]:
        """List[Tile]: the possible adjacent tiles to a given tile that can be added as new tiles to the tiling

        Args:
            other (Tile): tile to check
        """
        return [
            tile
            for tile in other.child_tiles
            if not self.contains(tile)
            and not self.overlap(tile)
            and self.consistent(tile)
        ]

    @property
    def max_coord(self) -> float:
        """float: maximum coordinate value of tile centers"""
        max_coord = 1
        for tile in self.tiles:
            if tile.center.max_abs_coord + 1 > max_coord:
                max_coord = tile.center.max_abs_coord + 1
        return max_coord

    def plot(
        self,
        size: float = 5,
        extent: float = 10,
        alpha_tile: float = 1,
        alpha_dual: Optional[float] = None,
    ) -> Figure:
        """
        plot the tiling

        Args:
            size (float, optional): size of the plot. Defaults to 5.
        """
        fig, ax = plot_setup(extent=extent, size=size)
        artists = self.artists(ax, alpha=alpha_tile)
        if alpha_dual is not None:
            centers = self.artist_centers(ax=ax, alpha=alpha_dual)
            edges = self.artist_edges(ax=ax, alpha=alpha_dual)
        return fig

    def artists(self, ax: Axes, alpha: float = 1) -> List[PathPatch]:
        """draw tiles on a given axis

        Args:
            ax (Axes): axis to draw on

        Returns:
            List[PathPatch]: result of add_patch for each tile
        """
        return [
            ax.add_patch(tile.patch(alpha=alpha)) for i, tile in enumerate(self.tiles)
        ]

    def artist_centers(self, ax: Axes, alpha: float = 1) -> PathCollection:
        return artist_centers(tiles=self.tiles, ax=ax, alpha=alpha)

    def artist_edges(self, ax: Axes, alpha: float = 1, tiles: Optional[List[Tile]] = None) -> LineCollection:
        lines = []
        tiles = self.tiles if tiles is None else tiles
        for tile in tiles:
            for child in tile.child_tiles:
                if self.contains(child):
                    lines.append(
                        [
                            [tile.center.x, tile.center.y],
                            [child.center.x, child.center.y],
                        ]
                    )
        return ax.add_collection(
            LineCollection(
                lines,
                color=plt.cm.Dark2.colors[3],
                zorder=1,
                alpha=alpha,
                lw=3,
            )
        )

    @property
    def as_json(self) -> List[Dict]:
        """List[Dict]: the tiling as a json (list of the tile jsons)"""
        return [tile.as_json for tile in self.tiles]


class NoTileAddable(Exception):
    """there is no way to add a compatible child tile at some place in a tiling"""

    pass