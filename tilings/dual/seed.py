"""seed tilings: square, triangle and hexagon
"""

from typing import NamedTuple, List, Optional
from enum import Enum
from .tile import Tile, Square, Triangle
from .point import Point
from .tiling import Tiling
import numpy as np
import random


class SeedParams(NamedTuple):
    tiles: List[Tile]
    num_sides_: Optional[int] = None


class Seed(SeedParams, Enum):
    SQUARE = SeedParams(tiles=[Square()])
    TRIANGLE = SeedParams(tiles=[Triangle()])
    HEXAGON = SeedParams(
        tiles=[
            Triangle(
                center=Point(x=0, y=1 / np.sqrt(3)), vertex=Point(x=0, y=0)
            ).rotate_around_origin(i * np.pi / 3)
            for i in range(6)
        ],
        num_sides_=6,
    )

    @property
    def num_sides(self) -> int:
        """number of sides

        Returns:
            int: number of sides
        """
        if self.num_sides_ is None:
            return self.tiles[0].num_sides
        else:
            return self.num_sides_

    @property
    def angle_symmetry(self) -> float:
        return 2 * np.pi / self.num_sides

    @property
    def tiling(self) -> Tiling:
        return Tiling(tiles=self.tiles)


def get_random_seed() -> Seed:
    return random.choice(list(Seed))