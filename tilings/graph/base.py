"""base tools for a tiling graph
"""

from dataclasses import dataclass
from typing import TypeVar, List, Optional, Set, Tuple
import numpy as np
import itertools


@dataclass
class Point:
    x: float
    y: float
    edges: Optional[List[TypeVar("Edge")]] = None
    faces: Optional[List[TypeVar("Face")]] = None

    def addEdge(self, edge: TypeVar("Edge")) -> None:
        if self.edges is None:
            self.edges = [edge]
        else:
            self.edges.append(edge)

    def addFace(self, face: TypeVar("Face")) -> None:
        if self.faces is None:
            self.faces = [face]
        else:
            self.faces.append(face)

@dataclass
class Edge:
    points: Optional[List[Point]] = None
    faces: Optional[List[TypeVar("Face")]] = None

    def __post_init__(self):
        assert len(self.points) <= 2, f"too many points for an edge: {self.points}"

    def addPoint(self, point: Point) -> None:
        if self.points is None:
            self.points = [point]
        else:
            assert (
                len(self.points) < 2
            ), f"can't add a point to this edge, already has two: {self.points}"
            self.points.add(point)

    def addFace(self, face: TypeVar("Face")) -> None:
        if self.faces is None:
            self.faces = [face]
        else:
            self.faces.add(face)


@dataclass
class Face:
    center: Optional[Point] = None
    points: Optional[List[Point]] = None
    edges: Optional[List[Edge]] = None

    def addEdge(self, edge: Edge) -> None:
        if self.edges is None:
            self.edges = [edge]
        else:
            self.edges.add(edge)

    def addPoint(self, point: Point) -> None:
        if self.points is None:
            self.points = [point]
        else:
            self.points.add(point)


@dataclass
class Triangle(Face):
    def __post_init__(self):
        assert (
            self.points is None or len(self.points) <= 3
        ), f"too many points for a triangle: {self.points}"
        assert (
            self.edges is None or len(self.edges) <= 3
        ), f"too many edges for a triangle: {self.edges}"


@dataclass
class Square(Face):
    def __post_init__(self):
        assert (
            self.points is None or len(self.points) <= 4
        ), f"too many points for a square: {self.points}"
        assert (
            self.edges is None or len(self.edges) <= 4
        ), f"too many edges for a square: {self.edges}"


@dataclass
class Tiling:
    fistFace: Face


def avg_point(points: List[Point]) -> Point:
    return Point(x=np.mean([p.x for p in points]), y=np.mean([p.y for p in points]))


def first_triangle() -> Triangle:
    p1 = Point(0, 0)
    p2 = Point(-0.5, np.sqrt(3) / 2)
    p3 = Point(0.5, np.sqrt(3) / 2)
    e1 = Edge(points=[p1, p2])
    e2 = Edge(points=[p2, p3])
    e3 = Edge(points=[p3, p1])
    points = [p1, p2, p3]
    edges = [e1, e2, e3]
    triangle = Triangle(center=avg_point(points), points=points, edges=edges)
    p1.addEdge(e1)
    p1.addEdge(e3)    
    p2.addEdge(e1)
    p2.addEdge(e2)
    p3.addEdge(e2)
    p3.addEdge(e3)
    for i in [p1, p2, p3, e1, e2, e3]:
        i.addFace(triangle)
    return triangle
