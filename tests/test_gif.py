from tilings.dual import base as b
import os


def test_gif(tmp_path):
    tiling_seq = b.TilingSeq()
    tiling_seq.add_tilings(num_steps=20)
    fp = f"{tmp_path}/test.gif"
    tiling_seq.save_gif(fp, num_tilings=10)
    assert os.path.isfile(fp)