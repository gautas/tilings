# %% tags=['hide-cell']
from IPython import get_ipython

if get_ipython() is not None:
    get_ipython().run_line_magic("load_ext", "autoreload")
    get_ipython().run_line_magic("autoreload", "2")


# %%
from tilings.dual import sequence
from tilings.dual.tiling import Tiling
from tilings.dual.utils import plot_setup, artist_centers
from matplotlib.animation import ArtistAnimation, PillowWriter

# %%
tiling_seq = sequence.TilingSeq(symmetric=True)
tiling_seq.add_tilings(num_steps=12)

# %%
tiles_new_all = []
for i, tiling in enumerate(tiling_seq.tilings):
    tiles_new = []
    if i == 0:
        tiles_new.extend(tiling.tiles)
    else:
        for tile in tiling.tiles:
            if not tiling_seq.tilings[i - 1].contains(tile):
                tiles_new.append(tile)
    tiles_new_all.append(tiles_new)

# %%
fig, ax = plot_setup(extent=4, size=5)

# %%
artists = []
for i in range(12):
    artists.append(
        tiling_seq.tilings[i].artists(ax=ax, alpha=0.5)
        + Tiling(tiles=tiles_new_all[i]).artists(ax=ax)
        + (
            [
                artist_centers(tiles=tiles_new_all[i], ax=ax, alpha=1),
                tiling_seq.tilings[i].artist_edges(
                    ax=ax, alpha=1, tiles=tiles_new_all[i]
                ),
            ]
        )
    )


# %%

ani = ArtistAnimation(fig, artists + artists[::-1])

# %%
writer = PillowWriter(fps=0.75)
ani.save("./book/images/centers.gif", writer=writer)