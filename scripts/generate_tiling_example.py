# %% tags=['hide-cell']
from IPython import get_ipython

if get_ipython() is not None:
    get_ipython().run_line_magic("load_ext", "autoreload")
    get_ipython().run_line_magic("autoreload", "2")

# %%
from tilings.dual import sequence
import matplotlib.pyplot as plt

tiling_seq = sequence.TilingSeq(symmetric=True)
tiling_seq.add_tilings(num_steps=30)

# %%
fig = tiling_seq.tilings[-1].plot(size=4, extent=9)


# %%
fig.savefig("./book/images/tiling.png", bbox_inches="tight", dpi=100)

# %%
