# %% tags=['hide-cell']
from IPython import get_ipython

if get_ipython() is not None:
    get_ipython().run_line_magic("load_ext", "autoreload")
    get_ipython().run_line_magic("autoreload", "2")


# %%
from tilings.dual import sequence, tiling, tile
from tilings.dual.utils import plot_setup, artist_centers
from matplotlib.animation import ArtistAnimation, PillowWriter

# %%
t = tiling.Tiling(tiles=[tile.Triangle()])

# %%
t2 = t.add(tiles=[t.tiles[-1].child_tiles_by_vertex[0][1]]).add(
    tiles=[t.tiles[-1].child_tiles_by_vertex[1][1]]
)
t2.plot()

# %%
t3 = t2.add(tiles=[t2.tiles[-1].child_tiles_by_vertex[1][1]])
t3.plot()

# %%
base = t3.add(tiles=[t3.tiles[-1].child_tiles_by_vertex[0][1]])
base.plot()

# %%
base_w_square = base.add(tiles=[base.tiles[-3].child_tiles_by_vertex[0][1]])
base_w_square.plot()

# %%
base_w_trianges = base.add(tiles=[base.tiles[-3].child_tiles_by_vertex[0][0]]).add(
    tiles=[base.tiles[1].child_tiles_by_vertex[1][0]]
)
base_w_trianges.plot()

# %%
fig, ax = plot_setup(extent=3, size=5)

# %%
artists = [
    base.artists(ax=ax),
    base_w_square.artists(ax=ax),
    base.artists(ax=ax),
    base_w_trianges.artists(ax=ax),
    base.artists(ax=ax),
]

# %%
ani = ArtistAnimation(fig, artists + artists[::-1])

# %%
writer = PillowWriter(fps=0.75)
ani.save("./book/images/stuck.gif", writer=writer)

# %%
