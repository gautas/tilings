"""makes a tiling gif
"""

# %% tags=['hide-cell']
from IPython import get_ipython

if get_ipython() is not None:
    get_ipython().run_line_magic("load_ext", "autoreload")
    get_ipython().run_line_magic("autoreload", "2")
    get_ipython().run_line_magic("matplotlib", "inline")


# %%
from tilings.dual import sequence
import argparse

# %%
parser = argparse.ArgumentParser(description="Make a tiling gif")

parser.add_argument(
    "--fp",
    type=str,
    help="file path to save gif to",
    default="tiling.gif",
)

parser.add_argument(
    "--symmetric",
    action="store_true",
    help="if included, tilings will be symmetric around the first tile",
)


parser.add_argument(
    "--num_steps",
    type=int,
    help="number of tile addition steps",
    default=70,
)

parser.add_argument(
    "--num_tilings",
    type=int,
    help="number of tiling frames to include in the animation",
    default=70,
)


parser.add_argument(
    "--fp_cred_gcs",
    type=str,
    help="path to google cloud storage credentials json",
    default=None,
)
parser.add_argument(
    "--bucket_name",
    type=str,
    help="name of google cloud storage bucket to store gif and metadata",
    default=None,
)

parser.add_argument(
    "--twitter_api_key",
    type=str,
    help="Twitter API key",
    default=None,
)

parser.add_argument(
    "--twitter_api_secret_key",
    type=str,
    help="Twitter API secret key",
    default=None,
)

parser.add_argument(
    "--twitter_access_token",
    type=str,
    help="Twitter access token",
    default=None,
)

parser.add_argument(
    "--twitter_access_secret_token",
    type=str,
    help="Twitter access secret token",
    default=None,
)


# %%
if __name__ == "__main__":
    args = parser.parse_args()

    tiling_seq = sequence.TilingSeq(symmetric=args.symmetric)

    # %%
    tiling_seq.add_tilings(num_steps=args.num_steps)

    # %%
    tiling_seq.save_gif(fp=args.fp, num_tilings=args.num_tilings)

    # %%
    tiling_seq.setup_bucket(
        fp_cred_gcs=args.fp_cred_gcs,
        bucket_name=args.bucket_name,
    )

    # %%
    tiling_seq.setup_twitter(
        twitter_api_key=args.twitter_api_key,
        twitter_api_secret_key=args.twitter_api_secret_key,
        twitter_access_token=args.twitter_access_token,
        twitter_access_secret_token=args.twitter_access_secret_token,
    )

    # %%
    tiling_seq.store_gif()

    # %%
    tiling_seq.tweet_gif()

    # %%
    tiling_seq.store_meta()

    # %%
    tiling_seq.store_tweet()