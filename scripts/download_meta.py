from tilings.dual.bucket import Bucket

# %%
bucket = Bucket()

bucket.setup_bucket(fp_cred_gcs="./keys/key.json", bucket_name="tilings")

# %%
bucket.download_meta()