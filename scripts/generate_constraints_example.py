# %% tags=['hide-cell']
from IPython import get_ipython

if get_ipython() is not None:
    get_ipython().run_line_magic("load_ext", "autoreload")
    get_ipython().run_line_magic("autoreload", "2")

# %%
import networkx as nx
import matplotlib.pyplot as plt

# %%
fig, ax = plt.subplots(figsize=(10, 10), nrows=2, ncols=2)

# %%
g1 = nx.Graph()
g1.add_edges_from([("S1", i) for i in ["T1", "T2", "S2", "S3"]])


# %%
nx.draw_spring(g1, ax=ax[0][0], with_labels=True, node_color="white", font_size=18)

# %%
g2 = nx.Graph()
g2.add_edges_from(
    [
        ("T3", i) for i in ['T4', 'S4', 'T5']
    ]
)


# %%
nx.draw_spring(g2, ax=ax[0][1], with_labels=True, node_color="white", font_size=18)

# %%
g3 = nx.Graph()
g3.add_edges_from(
    [("S5", f"S{i}") for i in range(6, 10)]
    + [(f"S{i}", "S10") for i in range(6, 10)]
)


# %%
nx.draw_circular(g3, ax=ax[1][0], with_labels=True, node_color="white", font_size=18)

# %%
g4 = nx.Graph()

# %%
nx.draw(g4, ax=ax[1][1])


# %%
ax[0][0].set_title('OK', fontsize=20)
ax[0][1].set_title('OK', fontsize=20)
ax[1][0].set_title('not OK', fontsize=20)

# %%
fig.savefig("./book/images/constraints.png", bbox_inches="tight", dpi=200)