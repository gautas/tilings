# %% tags=['hide-cell']
from IPython import get_ipython

if get_ipython() is not None:
    get_ipython().run_line_magic("load_ext", "autoreload")
    get_ipython().run_line_magic("autoreload", "2")


# %%
from tilings.dual import sequence
from tilings.dual.utils import plot_setup
from matplotlib.animation import ArtistAnimation, PillowWriter

# %%
tiling_seq = sequence.TilingSeq(symmetric=True)
tiling_seq.add_tilings(num_steps=10)

# %%
tiling = tiling_seq.tilings[-1]

# %%
fig, ax = plot_setup(extent=4, size=5)

# %%
artists = []
for i in range(21):
    print(i)
    # tiling.plot(extent=5, alpha_tile=i / 10, alpha_dual=1 - i / 10)
    artists.append(
        tiling.artists(ax=ax, alpha=(i + 10) / 30)
        + (
            [
                tiling.artist_centers(ax=ax, alpha=1 - i / 30),
                tiling.artist_edges(ax=ax, alpha=1 - i / 30),
            ]
        )
    )


# %%

ani = ArtistAnimation(fig, artists + artists[::-1])

# %%
writer = PillowWriter(fps=10)
ani.save("./book/images/dual.gif", writer=writer)

# %%
