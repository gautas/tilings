"""
download all tweets
"""

# %% tags=['hide-cell']
from IPython import get_ipython
if get_ipython() is not None:
    get_ipython().run_line_magic('load_ext', 'autoreload')
    get_ipython().run_line_magic('autoreload', '2')


# %%
import json
from tilings.dual import sequence
from tweepy import Cursor
from tqdm import tqdm
import pandas as pd

# %%
with open('./../keys/twitter.json', 'r') as f:
    keys = json.load(f)

# %%
tiling_seq = sequence.TilingSeq()

# %%
tiling_seq.setup_twitter(twitter_api_key=keys['API key'], twitter_api_secret_key=keys['API secret key'], twitter_access_token=keys['Access token'], twitter_access_secret_token=keys['Access token secret'])

# %%
statuses = []
for status in tqdm(Cursor(tiling_seq.api.user_timeline).items(400)):
    statuses.append({key: status._json[key] for key in ['created_at', 'id', 'retweet_count', 'favorite_count']})

# %%
len(statuses)

# %%
df_status = pd.DataFrame(statuses)

# %%
df_status

# %%
df_status.favorite_count.sum()

# %%
df_status[df_status.favorite_count > 0]

# %%
pd.to_datetime(df_status['created_at']).max()

# %%
pd.to_datetime(df_status['created_at']).min()


# %%
