from tilings.dual.slides import Example, bucket
from tilings.dual.bucket import TypeBlob, config
import argparse
import logging
from shutil import copy
from pathlib import Path

logging.basicConfig(level=logging.INFO)

parser = argparse.ArgumentParser(description="Download gif tweet examples")

parser.add_argument(
    "--fp_cred_gcs",
    type=str,
    help="path to google cloud storage credentials json",
    default="./keys/key.json",
)

parser.add_argument(
    "--bucket_name",
    type=str,
    help="name of google cloud storage bucket to store gif and metadata",
    default="tilings",
)

args = parser.parse_args()

bucket.setup_bucket(
    fp_cred_gcs=args.fp_cred_gcs,
    bucket_name=args.bucket_name,
)

# %%
config.set_loc_download("./bucket")

locs_image = [
    # "./book/_build/html/images/",
    "./book/images/",
]
for loc in locs_image:
    Path(loc).mkdir(parents=True, exist_ok=True)

for example in Example:
    example.folder.get(TypeBlob.GIF).download()
    for loc in locs_image:
        copy(example.folder_local.fp_gif_local, loc)