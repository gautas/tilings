# %% [markdown]
"""
# Test twitter upload
"""

# %% tags=['hide-cell']
from IPython import get_ipython

if get_ipython() is not None:
    get_ipython().run_line_magic("load_ext", "autoreload")
    get_ipython().run_line_magic("autoreload", "2")
    get_ipython().run_line_magic("matplotlib", "inline")


# %%
import tweepy
import json


# %%
with open("./../keys/twiiter.json") as f:
    keys = json.load(f)

# %%
keys

# %%
auth = tweepy.OAuthHandler(keys["API key"], keys["API secret key"])

# %%
auth.set_access_token(keys["Access token"], keys["Access token secret"])

# %%
api = tweepy.API(auth)

# %%
a = api.update_status("test2")

# %%
type(a)

# %%
str(a._json)

# %%
b = api.update_with_media("tiling.gif")

# %%
b._json

# %%
